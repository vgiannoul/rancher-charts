apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  name: db
  labels:
    io.kompose.service: php
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
spec:
  ports:
  - name: "3306"
    port: 3306
    targetPort: 3306
  selector:
    io.kompose.service: db
status:
  loadBalancer: {}

---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: php
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: php
spec:
  ports:
  - name: php-fpm
    port: 9000
    targetPort: 9000
  selector:
    io.kompose.service: php
status:
  loadBalancer: {}

---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: redis
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: redis
spec:
  ports:
  - name: "6379"
    port: 6379
    targetPort: 6379
  selector:
    io.kompose.service: redis
status:
  loadBalancer: {}

---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: nginx
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: nginx
spec:
  ports:
  - name: "80"
    port: 80
    targetPort: 80
    protocol: TCP
  selector:
    io.kompose.service: nginx
status:
  loadBalancer: {}

---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: crond
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: crond
spec:
  ports:
  - name: "22"
    port: 22
    targetPort: 22
  selector:
    io.kompose.service: crond
status:
  loadBalancer: {}

---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: sshd
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: sshd
spec:
  ports:
  - name: "22"
    port: 22
    targetPort: 22
  selector:
    io.kompose.service: sshd
status:
  loadBalancer: {}

---

apiVersion: v1
kind: Service
metadata:
  name: sshd-pub
  labels:
    name: sshd-pub
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
spec:
  type: NodePort
  ports:
  - port: 22
  selector:
    io.kompose.service: sshd

{{- if eq .Values.node.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  name: node
  labels:
    name: node
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
spec:
  ports:
  - name: "3000"
    port: 3000
    targetPort: 3000
  selector:
    io.kompose.service: node
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.pma.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  name: pma
spec:
  clusterIP: None
  ports:
  - name: "80"
    port: 80
    targetPort: 80
  selector:
    io.kompose.service: pma
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.solr.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  name: solr
spec:
  clusterIP: None
  ports:
  - name: headless
    port: 55555
    targetPort: 0
  selector:
    io.kompose.service: solr
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.elasticsearch.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  name: elasticsearch
spec:
  ports:
  - name: "9200"
    port: 9200
    targetPort: 9200
  selector:
    io.kompose.service: elasticsearch
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.varnish.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: varnish
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: varnish
spec:
  ports:
  - name: "6081"
    port: 6081
    targetPort: 6081
    protocol: TCP
  - name: "6082"
    port: 6082
    targetPort: 6082
    protocol: TCP
  selector:
    io.kompose.service: varnish
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.opensmtpd.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: opensmtpd
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: opensmtpd
spec:
  ports:
  - name: "25"
    port: 25
    targetPort: 25
    protocol: TCP
  selector:
    io.kompose.service: opensmtpd
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.kibana.enabled true }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: kibana
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: kibana
spec:
  ports:
  - name: "5601"
    port: 5601
    targetPort: 5601
    protocol: TCP
  selector:
    io.kompose.service: kibana
status:
  loadBalancer: {}
{{- end }}

{{- if eq .Values.nginx.modpagespeed.cdn "enabled" }}
---

apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    io.kompose.service: nginx-proxy
    app: "{{ template "drupal8.fullname" . }}"
    release: {{ .Release.Name | quote }}
    heritage: {{ .Release.Service | quote }}
  name: nginx-proxy
spec:
  ports:
  - name: "80"
    port: 80
    targetPort: 80
    protocol: TCP
  selector:
    io.kompose.service: nginx-proxy
status:
  loadBalancer: {}
{{- end }}
