# Basic HTML static site
A simple html website.

## Introduction

This chart bootstraps a simple html deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager. It currently siupports NGINX but in the future you will also be able to choose APACHE

This chart has been tested to work with NGINX Ingress

## Upgrade Notes:

None yet.
